## Overview ##
**googlecalendarsync** is a tool written in python to bidirectional synchronize a local iCal (.ics) file with Google Calendar

## Requirements ##
  * python-vobject - for local iCal file management
  * python-gdata - for remote Google Calendar management
  * python-httplib2 - for remote iCal retrieval

## Install ##
  * Download the latest release [here](http://code.google.com/p/googlecalendarsync/downloads/list),
  * Extract the source tarball _googlecalendarsync-x.y.tar.gz_,
  * Run _python setup.py build_ to make,
  * Run _sudo python setup.py install_ to install.

## How to use googlecalendarsync ##

Create a configuration file (`$HOME/.googlecalsync/config`) using the following template:
```
[google]
username = <GOOGLE ACCOUNT USERNAME>
password = <GOOGLE ACCOUNT PASSWORD>

[local]
ical_file = <PATH OF THE LOCAL iCal FILE>
workdir = ${HOME}/.googlecalsync/work/
```

Then run:
```
  googlecalsync.py [--dry-run]
```

### Recommended usage ###
Add an entry in crontab (as non-privileged user) like the following to synchronize a local calendar with Google Calendar every 30mins:
```
0,30 * * * * /usr/bin/googlecalsync.py >/dev/null 2>&1
```